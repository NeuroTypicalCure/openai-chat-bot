const { Client, GatewayIntentBits } = require('discord.js');
const client = new Client({ intents: [GatewayIntentBits.Guilds] });
require('./slashCommands');
const CONFIG = require('./config.json');
const { Configuration, OpenAIApi } = require('openai');

const configuration =  new Configuration({
  apiKey: CONFIG.OPENAI_API_KEY
});

const AI = new OpenAIApi(configuration);

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('interactionCreate', async interaction => {
  if (!interaction.isChatInputCommand()) return;

  // PONG
  if (interaction.commandName === 'ping') {
    await interaction.reply('Pong!');
  }

  // CHAT
  if(interaction.commandName === 'chat') {
    const prompt = interaction.options.getString('prompt');
    
    await interaction.deferReply();
    
    const completion = await AI.createCompletion({
      model: "text-davinci-002",
      prompt: prompt,
      temperature: 0,
      top_p: 1,
      max_tokens: 2048,
      echo: true
    });

		await interaction.editReply(completion.data.choices[0].text);
  }
});

client.login(CONFIG.TOKEN);